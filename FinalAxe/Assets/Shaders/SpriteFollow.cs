using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteFollow : MonoBehaviour
{

    SpriteRenderer myRenderer;
    public SpriteRenderer originalRenderer;
    // Start is called before the first frame update
    void Start()
    {
        myRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(originalRenderer.sprite != myRenderer.sprite)
        {
            myRenderer.sprite = originalRenderer.sprite;
        }
        //since the parent is flip when ealking left we have to flip the shadow too
        transform.localScale = new Vector3(Mathf.Abs( transform.localScale.x) * Mathf.Sign(originalRenderer.transform.localScale.x), transform.localScale.y, transform.localScale.z);
        if (originalRenderer.transform.localScale.x < 0)
        {
            myRenderer.flipX = true;
        }
        else
        {
            myRenderer.flipX = false;
        }

    }
}
