
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MyLight2D : MonoBehaviour
{
    //light radius
    [SerializeField]
    float radius;
    [SerializeField][Tooltip("The sphere radius out of which the shadow start to fade. Should always be lower thasn or equal to Radius")]
    float shadowDarknessRadius;
    //any object that casts shadow and intersecate the sphere will spawn a shadow, any object outside the sphere will not cast a shadow
    CircleCollider2D circle;

    [SerializeField]
    List<string> targetTag;

    [SerializeField]
    Material shadowMaterial;

    [SerializeField]
    Color shadowColor;

    float sceneSize;
    List<GameObject> currentShadowCasted = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        circle = gameObject.AddComponent<CircleCollider2D>();
        circle.isTrigger = true;
        float z = transform.position.z;
        circle.radius = Mathf.Sqrt(radius * radius - z * z);

        sceneSize = Camera.main.orthographicSize * 2.0f;
    }



    private void OnTriggerEnter2D(Collider2D other)
    {
        //when an object intersecate the collider it will spawn a shadow
        if (targetTag.Contains(other.gameObject.tag))
        {
            foreach (Transform child in other.gameObject.transform.GetComponentsInChildren<Transform>())
            {
                if (currentShadowCasted.Contains(child.gameObject))
                {
                    return;
                }
            }
            //shadow instantiate
            GameObject shadow = new GameObject("Shadow");
            shadow.transform.parent = other.gameObject.transform;
            SpriteRenderer shadowRenderer = shadow.AddComponent<SpriteRenderer>();
            SpriteRenderer originalRenderer = other.gameObject.GetComponent<SpriteRenderer>();
            shadowRenderer.sprite = originalRenderer.sprite;
            shadowRenderer.material = shadowMaterial;
            shadowRenderer.sortingLayerName = "Shadow";
            shadow.tag = "Shadow";
            shadow.transform.position = other.gameObject.transform.position;
            shadow.transform.rotation = Quaternion.identity;
            shadow.transform.localScale = new Vector3(1,1,1);
            shadow.AddComponent<SpriteFollow>().originalRenderer = originalRenderer;

            //set shaders parameters
            shadowRenderer.material.SetColor("ShadowColor", shadowColor);
            shadowRenderer.material.SetVector("LightPos", transform.position);
            shadowRenderer.material.SetFloat("ObjHeight", originalRenderer.bounds.size.y);
            shadowRenderer.material.SetFloat("MaxHeight", sceneSize);
            shadowRenderer.material.SetFloat("MaxDarknessRadius", shadowDarknessRadius);
            shadowRenderer.material.SetFloat("LightRadius", radius);

            currentShadowCasted.Add(shadow);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        //remove shadow from scene
        if (targetTag.Contains(other.gameObject.tag))
        {
            foreach (Transform child in other.gameObject.transform.GetComponentsInChildren<Transform>())
            {
                if (currentShadowCasted.Contains(child.gameObject))
                {
                    Destroy(child.gameObject);
                }
            }
        }
    }


    void OnDrawGizmosSelected()
    {
        
        Vector3 discsCenter = new Vector3(transform.position.x, transform.position.y, 0);
        float lightDiscRay = Mathf.Sqrt(radius * radius - transform.position.z * transform.position.z);
        float shadowDiscRay = Mathf.Sqrt(shadowDarknessRadius * shadowDarknessRadius - transform.position.z * transform.position.z);
        Handles.color = Color.yellow;
        Handles.DrawWireDisc(discsCenter, transform.forward, lightDiscRay);
        Handles.color = Color.black;
        Handles.DrawWireDisc(discsCenter, transform.forward, shadowDiscRay);

    }
}
