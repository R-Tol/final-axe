using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageableCharacter : Damageable
{
    Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public override void TakeDamage(float damageAmount)
    {
        animator.SetTrigger("Damaged");
        Debug.Log("hit");
        base.TakeDamage(damageAmount);
    }

    protected override void Die()
    {
        animator.SetTrigger("Die");
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }
}
