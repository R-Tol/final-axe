using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackingCharacter : MonoBehaviour
{
    public float attackDamage;

    bool isAttacking;

    public bool IsAttacking { get => isAttacking; 
        set 
        { 
            isAttacking = value;
            canAttack = !isAttacking;
            animator.SetBool("IsAttacking", isAttacking);
            attackStateChanged?.Invoke(value); //comunicate new attack state to listeners
        }
    }

    bool canAttack = true;

    Animator animator;

    public Action<bool> attackStateChanged;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void Attack()
    {
        if(canAttack)
            IsAttacking = true;
    }

    public void FinishedAttack()
    {
        IsAttacking = false;
    }

    public void DisableAttack()
    {
        canAttack = false;
    }

    public void EnableAttack()
    {
        canAttack = true;
    }
}
