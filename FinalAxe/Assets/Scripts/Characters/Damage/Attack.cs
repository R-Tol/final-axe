using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    AttackingCharacter owner;
    [SerializeField]
    LayerMask damageablesLayerMasks;

    private void Start()
    {
        if(!transform.parent.TryGetComponent<AttackingCharacter>(out owner))
        {
            Debug.LogError("Attacking Character component missing from parent");
        }
        owner.attackStateChanged += gameObject.SetActive; //set active when attacking
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.isTrigger && //prevent double collison with moving platform
            collision.gameObject != owner.gameObject && //check if the object is different
            damageablesLayerMasks == (damageablesLayerMasks | ( 1 << collision.gameObject.layer)) ) // check if the layer is the same of the damageables
        {
            collision.gameObject.GetComponent<Damageable>().TakeDamage(owner.attackDamage);
        }
    }

    private void OnDestroy()
    {
        owner.attackStateChanged -= gameObject.SetActive;
    }
}
