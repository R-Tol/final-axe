using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAIMovement : Movement
{
    public float chasingRange;
    public float attackRange;

    public GameObject player;
    [HideInInspector]
    public PathFinding pathFinding;

    AIStateMachine stateMachine;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        currentSpeed = normalSpeed;
        pathFinding = GetComponent<PathFinding>();
        stateMachine = GetComponent<AIStateMachine>();
        if(pathFinding == null)
        {
            Debug.LogError("Pathfinfinding component missing!");
        }
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        UpdateState();
    }

    public void UpdateState()
    {
        if (attackHandler.IsAttacking || player == null) //let attack animation finish or stop chasing if player died
            return;
        if (Vector3.Distance(transform.position, player.transform.position) > chasingRange) //patrol player
        {
            stateMachine.ChangeState(new PatrolAIState());
        }
        else
        {
            if (Vector3.Distance(transform.position, player.transform.position) > attackRange) //start the chase
            {
                stateMachine.ChangeState(new ChaseAIState());
            }
            else
            {
                stateMachine.ChangeState(new AttackAIState()); //start attack waiting time
            }
        }
    }

    public void Move() //find new direction
    {
        movement = pathFinding.GetDirection(transform, player.transform);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, chasingRange);
    }
}
