using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleChase : PathFinding
{
    public override Vector2 GetDirection(Transform start, Transform target)
    {
        Vector2 movement = new Vector2();
        Vector3 startPos = start.transform.position;
        if (Mathf.Abs(target.transform.position.y - start.transform.position.y) < 0.1f) //maintain same y if it's close to player's y
        {
            movement.y = 0;
        }
        else
        {
            movement.y = Mathf.Sign(target.transform.position.y - start.transform.position.y);
        }

        movement.x = Mathf.Sign(target.transform.position.x - start.transform.position.x);
        return movement; 
    }
}
