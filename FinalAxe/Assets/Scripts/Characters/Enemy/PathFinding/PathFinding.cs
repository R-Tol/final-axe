using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PathFinding : MonoBehaviour
{
    public abstract Vector2 GetDirection(Transform start, Transform target);
}
