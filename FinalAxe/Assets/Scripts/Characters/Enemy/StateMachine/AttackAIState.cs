using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackAIState : AIState
{
    Coroutine attackCoroutine;

    public override void OnEnterState()
    {
        attackCoroutine = owner.StartCoroutine(WaitAndAttack());
        owner.movementHandler.DisableMovement();//disable movement while attacking
    }

    public IEnumerator WaitAndAttack()
    {
        yield return new WaitForSeconds(owner.attackDelay);
        owner.attackHandler.attackStateChanged += OnAttackFinish; //register to attack finish to return to patrol state
        owner.attackHandler.Attack();
    }

    public override void OnExitState()
    {
        owner.attackHandler.attackStateChanged -= OnAttackFinish;
        owner.movementHandler.EnableMovement();
        owner.StopCoroutine(attackCoroutine);
    }

    public void OnAttackFinish(bool isAttackFinished)
    {
        if (isAttackFinished) //return to patrol state
        {
            owner.ChangeState(new PatrolAIState());
        }
    }
}
