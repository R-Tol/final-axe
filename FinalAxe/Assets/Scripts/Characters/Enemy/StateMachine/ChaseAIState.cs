using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseAIState : AIState
{
    public override void OnStateUpdate()
    {
        owner.movementHandler.Move();
    }
}
