using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AIState
{
    public AIStateMachine owner;

    //Called when the state machine enters this state
    public virtual void OnEnterState()
    {

    }

    //Called every frame when the state machine is in this state
    public virtual void OnStateUpdate()
    {

    }

    //Called when the state machine exit this state
    public virtual void OnExitState()
    {


    }
}
