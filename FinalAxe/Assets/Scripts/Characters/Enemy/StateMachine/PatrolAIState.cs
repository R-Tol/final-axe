using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolAIState : AIState
{
    public override void OnEnterState()
    {
        owner.movementHandler.DisableMovement();
    }

    public override void OnExitState()
    {
        owner.movementHandler.EnableMovement();
    }
}
