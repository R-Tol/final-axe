using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIStateMachine : MonoBehaviour//State Machine that handles AIStates
{
    private AIState currentState;
    
    [HideInInspector]
    public EnemyAIMovement movementHandler;
    [HideInInspector]
    public AttackingCharacter attackHandler;
    [Tooltip("seconds before starting attack animation after reaching the hero")]
    public float attackDelay;

    public AIState CurrentState
    {
        get => currentState;
        set
        {
            if (currentState != null)
            {
                currentState.OnExitState(); // call exit state on old state
            }
            currentState = value;
            currentState.owner = this;
            if (currentState != null)
            {
                currentState.OnEnterState(); //call enter state on new state
            }
        }
    }

    private void Start()
    {
        movementHandler = GetComponent<EnemyAIMovement>();
        attackHandler = GetComponent<AttackingCharacter>();
        ChangeState(new PatrolAIState()); // set starting state
    }

    private void Update()
    {
        currentState.OnStateUpdate(); //call StateMachine updates
    }

    

    public void ChangeState(AIState newState)
    {
        if (currentState == null || newState.GetType() != currentState.GetType()) // if we are entering a new state
        {
            CurrentState = newState;
        }
    }
}
