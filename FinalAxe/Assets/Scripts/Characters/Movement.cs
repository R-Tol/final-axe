using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    protected bool isInAir;
    protected bool canMove = true;
    protected Rigidbody2D rigidbody2d;
    protected Animator animator;

    [SerializeField]
    protected float normalSpeed;
    [SerializeField]
    protected float runSpeed;
    protected float currentSpeed;

    protected Vector2 movement; // movement firection vector

    protected SortingOrderUpdater sortingOrderUpdater;
    protected LayerMask layerMask;

    #region jump variables
    [SerializeField]
    [Tooltip("Speed at jump start")]
    protected float jumpInitialSpeed;
    [SerializeField]
    protected LayerMask pavementLayerMask;
    protected float jumpOffset;
    protected float currentJumpSpeed;
    [SerializeField]
    protected LayerMask airLayerMask;
    #endregion

    protected AttackingCharacter attackHandler;

    protected virtual void Start()
    {
        animator = GetComponent<Animator>();
        rigidbody2d = GetComponent<Rigidbody2D>();
        layerMask = gameObject.layer;
        sortingOrderUpdater = GetComponent<SortingOrderUpdater>();
        attackHandler = GetComponent<AttackingCharacter>();
    }

    protected virtual void Update()
    {
        if (isInAir) //Handle falls
        {
            //jump movement values
            currentJumpSpeed = currentJumpSpeed - 9.81f * Time.deltaTime;
            //can't go up because he is jumping
            movement.y = 0;
            //set animations parameters
            if (currentJumpSpeed > 0)
            {
                animator.SetBool("Jump", true);
                animator.SetBool("Fall", false);
            }
            else
            {
                animator.SetBool("Jump", false);
                animator.SetBool("Fall", true);
                RaycastHit2D raycastHit2D = Physics2D.Raycast(transform.position, new Vector2(0, -1.0f), 10000, pavementLayerMask);
                if (raycastHit2D != null)
                {
                    //Land at the same offset or less in respect to the start jump point
                    if (jumpOffset >= raycastHit2D.distance)
                    {
                        isInAir = false;
                        gameObject.layer = layerMask;
                        currentJumpSpeed = 0;
                        sortingOrderUpdater.enabled = true;
                    }
                }
            }
        }
        //animate
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("Speed", Mathf.Max(Mathf.Abs(currentSpeed * movement.x), Mathf.Abs(currentSpeed * movement.y)));
        //change scale if moving left side to flip the sprite
        if (movement.x != 0 && !attackHandler.IsAttacking)
            transform.localScale = new Vector3(Mathf.Sign(movement.x), transform.localScale.y, transform.localScale.z);
    }


    public void DisableMovement()
    {
        canMove = false;
    }

    public void EnableMovement()
    {
        canMove = true;
    }

    protected void Jump()
    {
        //do not change sorting order while jumping
        sortingOrderUpdater.enabled = false;
        gameObject.layer = (int)Mathf.Log(airLayerMask.value, 2.0f);
        isInAir = true;
        RaycastHit2D raycastHit2D = Physics2D.Raycast(transform.position, new Vector2(0, -1.0f), 10000, pavementLayerMask);
        if (raycastHit2D != null)
        {
            //Save an offset to know where to land
            jumpOffset = raycastHit2D.distance;
            currentJumpSpeed = jumpInitialSpeed;
            movement.y = currentJumpSpeed;
        }
    }

    protected virtual void FixedUpdate()
    {
        if (!canMove)
        {
            rigidbody2d.velocity = new Vector2();
        }
        else
        {
            rigidbody2d.velocity = movement * currentSpeed;
        }

        rigidbody2d.velocity += new Vector2(0, currentJumpSpeed);
    }
}
