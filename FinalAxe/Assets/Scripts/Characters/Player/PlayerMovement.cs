using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : Movement
{

    protected override void Update()
    {
        //x movement
        
        movement.x = Input.GetAxisRaw("Horizontal");

        //y movement

        if (!isInAir)
        {
            movement.y = Input.GetAxisRaw("Vertical");
            //reset animation parameters
            animator.SetBool("Jump", false);
            animator.SetBool("Fall", false);

            if (Input.GetKeyDown(KeyCode.Space) && canMove)
            {
                Jump();
            }
            else
            {
                //handle run
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    currentSpeed = runSpeed;
                }
                else
                {
                    currentSpeed = normalSpeed;
                }
            }

        }

        if (Input.GetKey(KeyCode.Q))
        {
            attackHandler.Attack();
        }

        base.Update();
        
    }

    
}
