using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBG : MonoBehaviour
{
    [SerializeField]
    Transform followTransform;
    Vector3 lastFollowPosition;
    Transform cameraTransform;
    Vector3 lastCameraPosition;

    float textureUnitSizeX;
    [SerializeField]
    Vector2 parallaxCoefficient;
    
    void Start()
    {
        //Get camera transform
        Camera camera = Camera.main;
        cameraTransform = camera.transform;

        lastCameraPosition = camera.transform.position;
        lastFollowPosition = followTransform.position;

        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        float screenWidth = camera.orthographicSize * 2.0f * camera.aspect;
        spriteRenderer.size = new Vector2(screenWidth * 3.0f, spriteRenderer.size.y);

        Sprite sprite = spriteRenderer.sprite;
        Texture2D texture = sprite.texture;

        textureUnitSizeX = texture.width / sprite.pixelsPerUnit;
    }

    void LateUpdate()
    {
        if (cameraTransform.position.x == lastCameraPosition.x)
            return;
        Vector3 cameraMovement = followTransform.position - lastFollowPosition;
        transform.position += new Vector3(cameraMovement.x * parallaxCoefficient.x, cameraMovement.y * parallaxCoefficient.y, 0);
        lastFollowPosition = followTransform.position;
        lastCameraPosition = cameraTransform.position;

        if(Mathf.Abs(followTransform.position.x - transform.position.x) >= textureUnitSizeX) //translate the image
        {
            float offsetPositionX = (followTransform.position.x - transform.position.x) % textureUnitSizeX;
            transform.position = new Vector3(followTransform.position.x + offsetPositionX, transform.position.y, 0);
        }
    }
}
